//go:generate stringer -type=Suit,Rank

package deck

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

// Suit represents all suits in deck of playing cards.
type Suit uint8

const (
	// Spade represents the first suit.
	Spade Suit = iota
	// Diamond represents the second suit.
	Diamond
	// Club represents the third suit.
	Club
	// Heart represents the fourth suit.
	Heart
	// Joker represents a special case card which has no suit.
	Joker
)

var suits = [...]Suit{Spade, Diamond, Club, Heart}

// Rank represents the value of each card in a deck of playing cards.
type Rank uint8

const (
	_ Rank = iota
	// Ace represents rank 1.
	Ace
	// Two represents rank 2.
	Two
	// Three represents rank 3.
	Three
	// Four represents rank 4.
	Four
	// Five represents rank 5.
	Five
	// Six represents rank 6.
	Six
	// Seven represents rank 7.
	Seven
	// Eight represents rank 8.
	Eight
	// Nine represents rank 9.
	Nine
	// Ten represents rank 10.
	Ten
	// Jack represents rank 11.
	Jack
	// Queen represents rank 12.
	Queen
	// King represents rank 13.
	King
)

const (
	minRank = Ace
	maxRank = King
)

// Card represents the suit and rank of a playing card.
type Card struct {
	Suit
	Rank
}

func (c Card) String() string {
	if c.Suit == Joker {
		return c.Suit.String()
	}

	return fmt.Sprintf("%s of %ss", c.Rank.String(), c.Suit.String())
}

// New creates a new Deck of playing cards.
func New(opts ...func([]Card) []Card) []Card {
	var cards []Card

	for _, suit := range suits {
		for rank := minRank; rank <= maxRank; rank++ {
			cards = append(cards, Card{Suit: suit, Rank: rank})
		}
	}

	for _, opt := range opts {
		cards = opt(cards)
	}

	return cards
}

// DefaultSort represents the default method for sorting a deck of playing cards.
func DefaultSort(cards []Card) []Card {
	sort.Slice(cards, Less(cards))
	return cards
}

// Sort represents a custom method for sorting a deck of playing cards.
func Sort(less func(cards []Card) func(i, j int) bool) func([]Card) []Card {
	return func(cards []Card) []Card {
		sort.Slice(cards, less(cards))
		return cards
	}
}

// Less ...
func Less(cards []Card) func(i, j int) bool {
	return func(i, j int) bool {
		return absRank(cards[i]) < absRank(cards[j])
	}
}

func absRank(c Card) int {
	return int(c.Suit)*int(maxRank) + int(c.Rank)
}

var shuffleRand = rand.New(rand.NewSource(time.Now().Unix()))

// Shuffle sorts the deck of playing cards into a random order.
func Shuffle(cards []Card) []Card {
	ret := make([]Card, len(cards))
	perm := shuffleRand.Perm(len(cards))

	for i, j := range perm {
		ret[i] = cards[j]
	}

	return ret
}

// Jokers adds a specified number of jokers to our deck of playing cards.
func Jokers(n int) func([]Card) []Card {
	return func(cards []Card) []Card {
		for i := 0; i < n; i++ {
			cards = append(cards, Card{
				Rank: Rank(i),
				Suit: Joker,
			})
		}

		return cards
	}
}

// Filter removed specified cards from the deck of playing cards.
func Filter(f func(card Card) bool) func([]Card) []Card {
	return func(cards []Card) []Card {
		var ret []Card

		for _, c := range cards {
			if !f(c) {
				ret = append(ret, c)
			}
		}

		return ret
	}
}

// Deck allows us to add a specified number of decks to our playing cards.
func Deck(n int) func([]Card) []Card {
	return func(cards []Card) []Card {
		var ret []Card

		for i := 0; i < n; i++ {
			ret = append(ret, cards...)
		}

		return ret
	}
}
